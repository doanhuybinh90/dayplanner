//
//  TasksTableViewController.swift
//  DayPlanner
//
//  Created by Doan Huy Binh on 9/22/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import CoreData

class TasksTableViewController: UITableViewController, NSFetchedResultsControllerDelegate{
    
    
    var AppDel = AppDelegate()
    var context = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDel = UIApplication.shared.delegate as! AppDelegate
        context = AppDel.persistentContainer.viewContext
        
        
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
      return (self.fetchedResultController.sections?.count)!
    }
    
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.fetchedResultController.sections!.count > 0
        {
            let sectionInfo = self.fetchedResultController.sections![section] as NSFetchedResultsSectionInfo
            return sectionInfo.name
        }
        else
        {
            return ""
        }
        
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.fetchedResultController.sections!.count > 0
        {
            let sectionInfo = self.fetchedResultController.sections![section] as NSFetchedResultsSectionInfo
            return sectionInfo.numberOfObjects
        }
        else
        {
            return 1
            
        }
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        
        // Configure the cell...
        self.configureCell(cell: cell, cellForRowAt: indexPath)
        return cell
    }
    
    
    func configureCell(cell: UITableViewCell, cellForRowAt indexPath: IndexPath)
    {
        let object = self.fetchedResultController.object(at: indexPath) as! NSManagedObject
        
        
        cell.textLabel?.text = (object.value(forKey: "taskDescription")! as AnyObject).debugDescription
        
        let taskStatus = (object.value(forKey: "taskStatus")! as AnyObject).debugDescription
        cell.detailTextLabel?.font = UIFont(name: "Avenir-Medium", size: 12.0)
        
        cell.detailTextLabel?.text = taskStatus
    }
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            let context = self.fetchedResultController.managedObjectContext
            
            context.delete(self.fetchedResultController.object(at: indexPath) as! NSManagedObject)
            
            do
            {
                try context.save()
            }
            catch _
            {
                let alertController = UIAlertController(title: "Error", message: "Error in Deleting The Record", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
            
        }
    }
    
    
    
    @IBAction func backPressed(_ sender: AnyObject) {
        
        let _ = navigationController?.popViewController(animated: true)
        
        
        
    }
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    
     // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let controller = segue.destination as! editTaskViewController
        
        if segue.identifier == "editTask"
        {
            
            if let IndexPath = self.tableView.indexPathForSelectedRow
            {
                let object = self.fetchedResultController.object(at: IndexPath) as! NSManagedObject
                
                controller.newTask = false
                controller.taskItem = object
                
            }
            
        } else if segue.identifier == "newTask"
        {
            controller.newTask = true
        }
        
        
    }
    
    
    //MARK: - Fetched Result Controller
    
    var _fetchedResultController : NSFetchedResultsController<NSFetchRequestResult>!
    var fetchedResultController : NSFetchedResultsController<NSFetchRequestResult>
    {
        if _fetchedResultController != nil
        {
            return _fetchedResultController
        }
        let fetchedRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Task")
        let entity = NSEntityDescription.entity(forEntityName: "Task", in: self.context)
        
        fetchedRequest.entity = entity
        
        fetchedRequest.fetchBatchSize = 20
        let sortDescriptor = NSSortDescriptor(key: "taskDateStamp", ascending: false)
        
        fetchedRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchedRequestController = NSFetchedResultsController(fetchRequest: fetchedRequest, managedObjectContext: self.context, sectionNameKeyPath: "taskDate", cacheName: nil)
        
        aFetchedRequestController.delegate = self
        _fetchedResultController = aFetchedRequestController
        
        do{
            try _fetchedResultController!.performFetch()
        }
        catch _
        {
            abort()
        }
        
        return _fetchedResultController
    }
    
    //MARK: - FRC - Delegate Method
    
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
    
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange sectionInfo: NSFetchedResultsSectionInfo, atSectionIndex sectionIndex: Int, for type: NSFetchedResultsChangeType) {
        
        switch type {
            
        case .insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        case .delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex) as IndexSet, with: .fade)
        default:
            return
        }
    }
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
           self.tableView.insertRows(at: [newIndexPath!], with: .fade)
        case .delete:
            self.tableView.deleteRows(at: [indexPath!], with: .fade)
        case .update:
            self.configureCell(cell: tableView.cellForRow(at: indexPath!)!,cellForRowAt: indexPath!)
        case .move:
            self.tableView.deleteRows(at: [indexPath!], with: .fade)
            self.tableView.insertRows(at: [newIndexPath!], with: .fade)
        default:
            return
        }
    }
}





