//
//  ViewController.swift
//  DayPlanner
//
//  Created by Doan Huy Binh on 9/22/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import CoreData


class ViewController: UIViewController {
    
    
    var appDel = AppDelegate()
    var context: NSManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
    
    @IBOutlet weak var todayPercentageLabel: UILabel!
    
    @IBOutlet weak var totalPercentageLabel: UILabel!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDel = UIApplication.shared.delegate as! AppDelegate
        context = appDel.persistentContainer.viewContext
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let today = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        let date = formatter.string(from: today as Date)

        let request: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "Task")
        request.resultType = NSFetchRequestResultType.dictionaryResultType
        
        var result : [AnyObject]?
        
        
        //Total Task
        do
        {
            result = try context.fetch(request)
        }
        catch _
        {
            let alertController = UIAlertController(title: "Error", message: "Error fetching Data", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        let total = result?.count
        
        //Total Complete
        
        request.predicate = NSPredicate(format: "taskStatus = %@", "Complete")
        
        do
        {
            result = try context.fetch(request)
        }
        catch _
        {
            let alertController = UIAlertController(title: "Error", message: "Error fetching Data", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        let totalComplete = result?.count
        
        //Total Today Task
        
        request.predicate = NSPredicate(format: "taskDate = %@",date)
        do
        {
            result = try context.fetch(request)
        }
        catch _
        {
            let alertController = UIAlertController(title: "Error", message: "Error fetching Data", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        let totalToday  = result?.count
        
        //Total ToDay Complete
        
        request.predicate = NSPredicate(format: "taskStatus = %@ && taskDate = %@", "Complete",date)
        do
        {
            result = try context.fetch(request)
        }
        catch _
        {
            let alertController = UIAlertController(title: "Error", message: "Error fetching Data", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        
        let totalTodayComplete = result?.count
        
        let totalPercentage = total! > 0 ? Int(Float(totalComplete!) / Float(total!) * 100): 0
        let todayPercentage = totalToday! > 0 ? Int(Float(totalTodayComplete!) / Float(totalToday!) * 100) : 0
        
        totalPercentageLabel.text = "\(totalPercentage)%"
        todayPercentageLabel.text = "\(todayPercentage)%"
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

