//
//  editTaskViewController.swift
//  DayPlanner
//
//  Created by Doan Huy Binh on 9/26/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import CoreData

class editTaskViewController: UIViewController, UITextFieldDelegate {
    
    
    var newTask = false
    var taskItem: AnyObject? = nil
    
    
    var appDel: AppDelegate = AppDelegate()
    var context:NSManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
    
    
    @IBOutlet weak var taskTextField: UITextField!
    
    @IBOutlet weak var taskStatusLabel: UILabel!
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBOutlet weak var statusSwitch: UISwitch!
    
    @IBOutlet weak var saveButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appDel = UIApplication.shared.delegate as! AppDelegate
        context = appDel.persistentContainer.viewContext
        
        taskTextField.delegate = self
        
        if newTask == true
        {
            saveButton.setTitle("Save", for: UIControlState.normal)
        }
        else
        {
            saveButton.setTitle("Update", for: UIControlState.normal)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if let task: AnyObject = self.taskItem
        {
            taskTextField.text = (task.value(forKey: "taskDescription")! as AnyObject).debugDescription
            let dateString = (task.value(forKey: "taskDate")! as AnyObject).debugDescription.description
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MM-yyyy"
            var date = formatter.date(from: dateString)
            date = date?.addingTimeInterval(18000)
            datePicker.setDate(date!, animated: true)
            
            if (task.value(forKey: "taskStatus")! as AnyObject).description == "Complete"
            {
                statusSwitch.isOn = true
                taskStatusLabel.text = "Complete"
            }
            else
            {
                statusSwitch.isOn = false
                taskStatusLabel.text = "Open"
            }
            
        }
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.resignFirstResponder()
        return true
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func statusSwtichChange(_ sender: AnyObject) {
        
        if sender.isOn == true
        {
            taskStatusLabel.text = "Complete"
        }
        else
        {
            taskStatusLabel.text = "Open"
        }
    }
    
    @IBAction func backPressed(_ sender: AnyObject) {
        
        let _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func savePressed(_ sender: AnyObject) {
        
        if taskTextField.text == ""
        {
            let alertController = UIAlertController(title: "Error", message: "You must provide a Task to Save Or Edit", preferredStyle:.alert)
            alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
        else
        {
            if saveButton.titleLabel?.text == "Save"
            {
                let date = datePicker.date
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                
                let newTask = NSEntityDescription.insertNewObject(forEntityName: "Task", into: context) as NSManagedObject
                newTask.setValue(taskTextField.text, forKey: "taskDescription")
                newTask.setValue(formatter.string(from: date), forKey: "taskDate")
                newTask.setValue(taskStatusLabel.text, forKey: "taskStatus")
                newTask.setValue(date, forKey: "taskDateStamp")
                
                do
                {
                    try context.save()
                    self.view.endEditing(true)
                    let alertController = UIAlertController(title: "Success", message: "Record Saved Successfully", preferredStyle:.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                    
                }
                catch _
                {
                    let alertController = UIAlertController(title: "Error", message: "Error in Saving Record", preferredStyle:.alert)
                    alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                    self.present(alertController, animated: true, completion: nil)
                }
            }
            else
            {
                if let task: AnyObject = self.taskItem
                {
                    let date = datePicker.date
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd-MM-yyyy"
                    
                    
                    task.setValue(taskTextField.text, forKey: "taskDescription")
                    task.setValue(formatter.string(from: date as Date), forKey: "taskDate")
                    task.setValue(taskStatusLabel.text, forKey: "taskStatus")
                    task.setValue(date, forKey: "taskDateStamp")
                    
                    do
                    {
                        try context.save()
                        self.view.endEditing(true)
                        let alertController = UIAlertController(title: "Error", message: "Record updated Successfully", preferredStyle:.alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                    catch _
                    {
                        let alertController = UIAlertController(title: "Error", message: "Error Updating Record", preferredStyle:.alert)
                        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}
/*
 // MARK: - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
 // Get the new view controller using segue.destinationViewController.
 // Pass the selected object to the new view controller.
 }
 */

